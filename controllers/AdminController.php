<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;

class AdminController extends Controller{

    /*
     *  Main admin action, list posts
     */
    public function actionAdmin()
    {
        return $this->render('admin');
    }
}