<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Post_translation;

class PostController extends Controller
{
    /*
     * Main page for user
     */
    public function actionIndex() {

        //Get languages, from DB
        $languagesArr = Yii::$app->languagecomponent->getLanguages();

        //Language from GET or SESSION
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $language = Yii::$app->languagecomponent->getLanguage($request, $session);

        //Cache
        $cache = Yii::$app->cache;

        //Querry table Post_translations
        $queryPosts = Post_translation::find();

        //Pagination
        $totalCount = $cache->getOrSet('countPosts', function () use ($queryPosts) {
            return $queryPosts->count();
        });
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $totalCount,
        ]);
        //Cache dependency
        $postDependency = $cache->getOrSet('clientDependency', function () {
            return time();
        });

        //Get posts from DB, cache
        $posts = $cache->getOrSet('postsPost_'.$language.'_'.$pagination->offset.'_'.$pagination->limit.'_'.$postDependency,
            function () use ($queryPosts, $language, $pagination) {
                return $queryPosts
                    ->where(['post_translation.language_id' => $language])
                    ->andWhere(['post_translation.status' => '1'])
                    ->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
            });

        //language for table view
        $tableLang = isset($posts[0]['language_id']) ? $posts[0]['language_id'] : '1';

        return $this->render('index', [
                'posts' => $posts,
                'languagesArr' => $languagesArr,
                'pagination' => $pagination,
                'tableLang' => $tableLang,
        ]);
    }

    /*
     * Detailed view for post
     */
    public function actionDetail (){

        //Get post id
        $request = Yii::$app->request;
        $get = $request->get();
        $id = $get['id'];

        //Language for get proper post from DB
        $session = Yii::$app->session;
        $language = Yii::$app->languagecomponent->getLanguage($request, $session);

        //Get languages
        $languagesArr = Yii::$app->languagecomponent->getLanguages();

        //Cache dependency
        $cache = Yii::$app->cache;
        $postDependency = $cache->getOrSet('clientDependency', function () {
            return time();
        });

        //Get post info from DB with Cache
        $post = $cache->getOrSet('postDetail_'.$id.'_'.$language.'_'.$postDependency, function () use ($id, $language) {
            return Post_translation::findOne([ 'post_id' => $id, 'language_id' => $language ]);
        });

        //check if result is empty and redirect to index page
        if(empty($post)){
            return $this->redirect( ['/post/index'] );
        }
        return $this->render('detail', [
            'post' => $post,
            'languagesArr' => $languagesArr
        ]);
    }
}