<?php

namespace app\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Post;
use app\models\Post_translation;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin']);
            //return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin']);
            //return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*
     * Admin action for list posts in admin page
     */
    public function actionAdmin()
    {
        //Check if we are logged
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        //Get languages from DB, for view choosing
        $languagesArr = Yii::$app->languagecomponent->getLanguages();

        //Language for get proper post from DB
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $language = Yii::$app->languagecomponent->getLanguage($request, $session);

        //Cache
        $cache = Yii::$app->cache;

        //Querry table Post_translations
        $queryPosts = Post_translation::find();

        //Pagination
        $totalCount = $cache->getOrSet('countPosts', function () use ($queryPosts) {
            return $queryPosts->count();
        });
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $totalCount,
        ]);

        //Cache dependency
        $postDependency = $cache->getOrSet('clientDependency', function () {
            return time();
        });

        //Get posts from DB, cache
        $posts = $cache->getOrSet('postsAdmin_'.$language.'_'.$pagination->offset.'_'.$pagination->limit.'_'.$postDependency,
            function () use ($queryPosts, $language, $pagination) {
                return $queryPosts
                    ->where(['post_translation.language_id' => $language])
                    ->andWhere(['post_translation.status' => '1'])
                    ->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
            });

        //language for table view
        $tableLang = isset($posts[0]['language_id']) ? $posts[0]['language_id'] : '1';
        
        return $this->render('admin', [
            'posts' => $posts,
            'languagesArr' => $languagesArr,
            'pagination' => $pagination,
            'tableLang' => $tableLang,
        ]);
    }
    /*
     * Admin action to create post
     */
    public function actionCreate()
    {
        //Check if we are logged
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        //Get languages from DB, for view
        $languagesArr = Yii::$app->languagecomponent->getLanguages();

        //Load models
        $modelPost = new Post();
        $modelTransl = new Post_translation();

        $post = Yii::$app->request->post();
        //When form is submitted
        if(isset($post['Post']) && isset($post['Post_translation'])){
            //Save data to Post table
            $modelPost->date_created = $post['Post']['date_created'];
            //Save in Post table and prepare data for Post_translation
            if($modelPost->save()){
                //last saved id in post table
                $lastId = $modelPost->id;
                //save data to Post_translation table
                $modelTransl->post_id = $lastId;
                $modelTransl->language_id = $post['Post_translation']['language_id'];
                $modelTransl->title = $post['Post_translation']['title'];
                $modelTransl->short_description = $post['Post_translation']['short_description'];
                $modelTransl->description = $post['Post_translation']['description'];
                $modelTransl->source = $post['Post_translation']['source'];
                $modelTransl->date_created = $post['Post']['date_created'];

                //Save in Post_translation table, after successful save in Post table
                if($modelTransl->save()){
                    return $this->redirect(['admin']);
                }else{
                    return $this->redirect(['error', 'message' => 'Error while save data in DB' ]);
                }
            }else{
                return $this->redirect(['error', 'message' => 'Error while save data in DB' ]);
            }
        }
        //reset cache dependency
        $cache = Yii::$app->cache;
        $cache->set('clientDependency', time());

        return $this->render('create', [
            'modelPost'=> $modelPost,
            'modelTransl'=> $modelTransl,
            'languagesArr' => $languagesArr
        ]);
    }
    /*
     * Admin action to edit post
     */
    public function actionEdit(){
        //Check if we are logged
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        //Get post_id, language
        $request = Yii::$app->request;
        $get = $request->get();
        $id = $get['id'];
        $languageId = $get['language_id'];

        //Get languages from DB, for view
        $languagesArr = Yii::$app->languagecomponent->getLanguages();
        //When form is submitted
        if( !empty(Yii::$app->request->post()) ){
            //Get post data
            $postEdit = Yii::$app->request->post();

            if(!isset($postEdit['edit_checkbox'])) {
                //update Post_translation table
                $postTransEdit = Post_translation::findOne($postEdit['Post_translation']['id']);
                $postTransEdit->post_id = $postEdit['Post_translation']['post_id'];
                $postTransEdit->language_id = $postEdit['Post_translation']['language_id'];
                $postTransEdit->title = $postEdit['Post_translation']['title'];
                $postTransEdit->short_description = $postEdit['Post_translation']['short_description'];
                $postTransEdit->description = $postEdit['Post_translation']['description'];
                $postTransEdit->source = $postEdit['Post_translation']['source'];
                //update
                if ($postTransEdit->save()) {
                    return $this->redirect(['admin']);
                }else{
                    return $this->redirect(['error', 'message' => 'Error while edit data in DB' ]);
                }
            }elseif($postEdit['edit_checkbox'] == 1){
                //new translation post
                $postTranslation = new Post_translation();
                $postTranslation->post_id = $postEdit['Post_translation']['post_id'];
                $postTranslation->language_id = $postEdit['Post_translation']['language_id'];
                $postTranslation->title = $postEdit['Post_translation']['title'];
                $postTranslation->short_description = $postEdit['Post_translation']['short_description'];
                $postTranslation->description = $postEdit['Post_translation']['description'];
                $postTranslation->source = $postEdit['Post_translation']['source'];
                $postTranslation->date_created = date(' j-m-y');
                //save new translalation
                if ($postTranslation->save()) {
                    return $this->redirect(['admin']);
                }else{
                    return $this->redirect(['error', 'message' => 'Error while save data in DB' ]);
                }
            }
        }
        //Load model
        $modelTransl = new Post_translation();

        //Get post info from DB, cache
        $cache = Yii::$app->cache;
        $post = $cache->getOrSet('postsSiteEdit', function () use ($id, $languageId) {
            return (new \yii\db\Query())
                    ->select([ 'id', 'post_id', 'language_id', 'title', 'short_description', 'description', 'source', 'date_created'])
                    ->from('post_translation')
                    ->where(['post_id' => $id])
                    ->andWhere(['language_id' => $languageId])
                    ->one();
        });
        //reset cache dependency
        $cache->set('clientDependency', time());

        return $this->render('edit', [
            'modelTransl'=> $modelTransl,
            'post' => $post,
            'languagesArr' => $languagesArr
        ]);
    }
    /*
     * Admin action to delete post
     */
    public function actionDelete(){

        //Check if we are logged
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        //Get post_id, language
        $request = Yii::$app->request;
        $get = $request->get();
        $id = $get['id'];

        //Get post for soft delete and update status field
        $postTranslation = Post_translation::findOne($id);
        $postTranslation->status = 0;
        //Soft delete
        if($postTranslation->save()){
            //reset cache dependency
            $cache = Yii::$app->cache;
            $cache->set('clientDependency', time());
            return $this->redirect(['admin']);
        }else{
            return $this->redirect(['error', 'message' => 'Error while delete data in DB' ]);
        }
    }
    /*
     * Detailed view for post
     */
    public function actionDetail (){

        //Check if we are logged
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        //Get post id
        $request = Yii::$app->request;
        $get = $request->get();
        $id = $get['id'];

        //Language for get proper post from DB
        $session = Yii::$app->session;
        $language = Yii::$app->languagecomponent->getLanguage($request, $session);

        //Get languages
        $languagesArr = Yii::$app->languagecomponent->getLanguages();

        //Get post info from DB with Cache
        $cache = Yii::$app->cache;
        $postDependency = $cache->getOrSet('clientDependency', function () {
            return time();
        });
        $post = $cache->getOrSet('postAdminDetail_'.$id.'_'.$language.'_'.$postDependency, function () use ($id, $language) {
            return Post_translation::findOne([ 'post_id' => $id, 'language_id' => $language ]);
        });

        //check if result is empty and redirect to index page
        if(empty($post)){
            return $this->redirect( ['/site/admin'] );
        }
        return $this->render('detail', [
            'post' => $post,
            'languagesArr' => $languagesArr
        ]);
    }
}
