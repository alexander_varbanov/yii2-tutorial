<?php

namespace app\commands;

use Yii;
use app\models\Post;
use app\models\Post_translation;
use yii\console\Controller;
use yii\console\ExitCode;

class PostController extends Controller
{
    protected $language_id = '1';
    protected $title = [ 'LoremIpsum360 ° представяне', 'Lorem Ipsum ползи', ' Lorem Ipsum използва',
                         'Лимити Lorem Ipsum', 'Lorem Ipsum произход', 'Lorem Ipsum, софтуер', ];
    protected $shortDescription = [ 'A Lipsum е текст, използван като заместител при липса на окончателния текст на модел или сайт.',
                                    'Едно от ограниченията на използването на фалшиви текст в уеб дизайна е, че този текст никога не се чете, 
                                    той не проверява действителното му разпознаваемост.',
                                    'Lorem Ipsum е стандарт, тъй като късното Средновековие.',
                                    'Много софтуерни инструменти предоставят произволни текстове пълнене да създаде фалшива съдържание директно в тяхната среда.',
                                    'LoremIpsum360 ° е безплатна онлайн генератор фалшива текст.',
                                    'В някои агенции в 90 разпространен текст, наречен "жълт трамвай" или "жълто метрото" разумно замени Lorem Ipsum 
                                    да даде по-модерен вид на съдържание.',
                                  ];
    protected $description = [ 'LoremIpsum360 ° е безплатна онлайн генератор фалшива текст. Той предоставя пълен текст симулатор за генериране 
                                променен текст или алтернативен текст за вашите модели. Тя се характеризира с различни случайни текстове на латински
                                 и вероятно да се генерира примери за текстове на различни езици.',
                                'В някои агенции в 90 разпространен текст, наречен "жълт трамвай" или "жълто метрото" разумно замени Lorem Ipsum да 
                                 даде по-модерен вид на съдържание. Но твърде много хора търсят да прочетат текста, когато тя беше на френски или английски език, 
                                 желаният ефект не се постига. Работа с четлив текст, съдържащ посоки може да предизвика разсейване и това може да помогне да се 
                                 фокусира върху оформлението.',
                                 'A Lipsum е текст, използван като заместител при липса на окончателния текст на модел или сайт. 
                                 Lipsum идва от свиването на най-известните алтернативни текстове: "" Lorem Ipsum "". Този вид текст се нарича още бял 
                                 текст, фалшив текст, невярно, попълнете текст променен текст.',
                                 'Едно от ограниченията на използването на фалшиви текст в уеб дизайна е, че този текст никога не се чете, той не 
                                 проверява действителното му разпознаваемост. Освен това формули проектирани с пробен текст са склонни да подценяват 
                                 пространството принуждавайки редакциите след това да опростенчески заглавия или неточна, че да не надвишава определеното пространство.',
                                 'Lorem Ipsum е стандарт, тъй като късното Средновековие. Художник ще има смесени части от текста в тях книга с примерни и го е този текст, 
                                 който ние ще използваме днес. Друга версия твърди, че това е откъс от книгата на Цицерон: "" De Finibus Bonorum и Malorum "" 
                                 секции 01.10.32 / 01.10.33. Тази книга, много популярен по време на Възраждането, е трактат по теория на етиката.',
                                 'Много софтуерни инструменти предоставят произволни текстове пълнене да създаде фалшива съдържание директно в тяхната среда. 
                                 Това може да бъде под формата на софтуер, за да инсталирате на вашия PC или Mac, или като разширения / плъгини за инсталиране в 
                                 съществуващия си софтуер (текстообработка / сваляне / на Web сайтове / дизайн ...).',
                              ];
    protected $source = [ 'bgloremipsum360.com', 'loremipsum360.com', 'bgloremipsum.com',
                          'loremipsum.com', 'bg360.com', 'ipsum360.com', ];
    protected $type = [ 'Новини', 'Спортни', 'Научни', 'Забавни', 'Документални', ];

    /*
     *  Add posts to my DB
     */
    public function actionIndex()
    {
        //for($i = 1; $i <= 400000; $i++) {
            //Random date
            $timestamp = rand(strtotime("Jan 01 2018"), strtotime("Jan 01 2022"));
            $dateCreated = date("d-m-Y", $timestamp);
            //Other random values
            $randTitle = array_rand($this->title, 1);
            $randShortDesc = array_rand($this->shortDescription, 1);
            $randDesc = array_rand($this->description, 1);
            $randSource = array_rand($this->source, 1);
            $randType = array_rand($this->type, 1);

            //Load models
            $modelPost = new Post();
            $modelTransl = new Post_translation();

            //Save data to Post table
            $modelPost->date_created = $dateCreated;

            //Save in Post table and prepare data for Post_translation
            if ($modelPost->save()) {
                //last saved id in post table
                $lastId = $modelPost->id;
                //save data to Post_translation table
                $modelTransl->post_id = $lastId;
                $modelTransl->language_id = $this->language_id;
                $modelTransl->title = $this->title[$randTitle];
                $modelTransl->short_description = $this->shortDescription[$randShortDesc];
                $modelTransl->description = $this->description[$randDesc];
                $modelTransl->source = $this->source[$randSource];
                $modelTransl->type = $this->type[$randType];
                $modelTransl->date_created = $dateCreated;

                //Save in Post_translation table, after successful save in Post table
                if ($modelTransl->save()) {
                    //return ExitCode::OK;
                } else {
                    //TODO Error handle, try ?
                    //return ExitCode::OK;
                }
            } else {
                //TODO Error handle, try ?
                //return ExitCode::OK;
            }
        //}
        return ExitCode::OK;
    }
}