-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Mar 30, 2022 at 11:17 AM
-- Server version: 8.0.28
-- PHP Version: 8.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `code` char(2) NOT NULL,
  `name` char(52) NOT NULL,
  `population` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`code`, `name`, `population`) VALUES
('AU', 'Australia', 24016400),
('BR', 'Brazil', 205722000),
('CA', 'Canada', 35985751),
('CN', 'China', 1375210000),
('DE', 'Germany', 81459000),
('FR', 'France', 64513242),
('GB', 'United Kingdom', 65097000),
('IN', 'India', 1285400000),
('RU', 'Russia', 146519759),
('US', 'United States', 322976000);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int NOT NULL,
  `label` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `label`, `name`) VALUES
(1, 'BG', 'Български'),
(2, 'EN', 'English'),
(3, 'DE', 'German');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int NOT NULL,
  `date_created` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `date_created`) VALUES
(1, '15-03-2022'),
(2, '15-03-2022'),
(11, '16-03-2022'),
(12, '17-03-2022'),
(13, '17-03-2022'),
(14, '17-03-2022'),
(15, '17-03-2022'),
(16, '18-03-2022');

-- --------------------------------------------------------

--
-- Table structure for table `post_translation`
--

CREATE TABLE `post_translation` (
  `id` int NOT NULL,
  `post_id` int NOT NULL,
  `language_id` int NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `source` varchar(255) NOT NULL,
  `date_created` varchar(60) NOT NULL,
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `post_translation`
--

INSERT INTO `post_translation` (`id`, `post_id`, `language_id`, `title`, `short_description`, `description`, `source`, `date_created`, `status`) VALUES
(1, 1, 1, 'Първа статия', 'Първата статия на български', '<p>Дълго описание на статията</p>', 'dnevnik.bg', '15-03-2022', '1'),
(2, 1, 2, 'First article', 'First Article in Bulgarian', 'Long description of the article', 'dnevnik.bg', '15-03-2022', '1'),
(3, 2, 1, 'Още една статия', 'Кратко описание', 'Дълго описание', 'abv.bg', '15-03-2022', '1'),
(4, 2, 2, 'One more article', 'Short description', 'Long description', 'abv.bg', '15-03-2022', '1'),
(7, 11, 1, 'Моят текст', 'Кратък тест 1', '<p>Дълъг тест 2</p>', 'www.abv.bg', '16-03-2022', '1'),
(9, 12, 1, 'Свързана статия', 'Статия за свързаните данни', '<p>Статия за свързаните данни и др.</p>', 'yii2.com', '17-03-2022', '1'),
(10, 13, 1, 'Тест 33', 'Тест, кратко', '<p>Тест, дълго описание</p>', 'БНТ', '17-03-2022', '1'),
(11, 14, 1, 'Заглавие ', 'Описание за 345', '<p>Дълго за 333</p>', '34', '18-03-2022', '1'),
(12, 15, 1, 'Поредна статия', 'Краткото съдържание', '<p>Дългото Съдържание</p>', 'няма', '17-03-2022', '1'),
(15, 11, 2, 'My text', 'Short text 1', '<p>Long text 2</p>', 'www.abv.bg', ' 18-03-22', '1'),
(16, 1, 3, 'Header doich', 'Kurze beschreibung', 'beschreibung', 'de.de', '22-03-2022', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `status`) VALUES
(1, 'admin', '', '$2y$13$VUfZVvwVZRSD5Gc2yWNSEe0gXfB3naRV9BcPtfzzY2TFFbmwS97qS', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_translation`
--
ALTER TABLE `post_translation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `post_translation`
--
ALTER TABLE `post_translation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
