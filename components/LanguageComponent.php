<?php
namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Language;
use yii\helpers\ArrayHelper;

class LanguageComponent extends Component{

    /*
     * Get languages from DB
     */
    public function getLanguages(){

        //Get languages from DB, for view, cache
        $cache = Yii::$app->cache;
        $languages = $cache->getOrSet('languagesDropDown', function () {
            $langQuery = Language::find();
            return $langQuery->all();
        });
        $languagesArr = [];
        $tempLanguages = ArrayHelper::toArray($languages);
        foreach ($tempLanguages as $key => $value){
            $languagesArr[$value['id']] = $value['label'];
        }
        return $languagesArr;
    }
    /*
     * Get language_id from GET or SESSION
     */
    public function getLanguage($request, $session){
        //Check if we have language in GET
        $get = $request->get();
        if (isset($get['language_id'])){
            $session->set('language', $get['language_id']);
            return $get['language_id'];
        }else {
            if($session->get('language') !== null){
                $language = $session->get('language');
            }else{
                $session->set('language', '1');
                $language = '1';
            }
        }
        return $language;
    }
}
