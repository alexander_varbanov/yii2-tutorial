<?php

namespace app\models;

use yii\db\ActiveRecord;

class Post_translation extends ActiveRecord
{
    public function rules()
    {
        //Required fields
        //Check if that post_id with specific language already exist
        return [
            [ [ 'title', 'short_description', 'description', 'source', 'date_created' ], 'required' ],
            [ ['post_id','language_id' ], 'unique', 'targetAttribute' => [ 'post_id', 'language_id' ], 'message' => 'This post already exists' ],
        ];
    }
}