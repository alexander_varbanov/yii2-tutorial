<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;
use codezeen\yii2\tinymce\TinyMce;

?>
<p><?= Html::a('<<<', ['/site/admin' ], ['class'=>'btn btn-info']) ?></p>
<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'edit-form']); ?>
            <!-- Id -->
            <?= $form->field($modelTransl, 'id')->hiddenInput( [ 'value' => $post['id'] ])->label(false) ?>
            <!-- Post id -->
            <?= $form->field($modelTransl, 'post_id')->hiddenInput( [ 'value' => $post['post_id'] ])->label(false) ?>
            <!-- Language -->
            <?= $form->field($modelTransl, 'language_id')->dropdownList($languagesArr,
                                 ['options' => [ $post['language_id'] => ['selected' => true] ]])->label('Language'); ?>
            <!-- Title -->
            <?= $form->field($modelTransl, 'title')->textInput([ 'value' => $post['title'] ])->label('Title') ?>
            <!-- Short Description -->
            <?= $form->field($modelTransl, 'short_description')->textarea([ 'value' => $post['short_description'] ]); ?>
            <!-- Description -->
            <?=  froala\froalaeditor\FroalaEditorWidget::widget([
                'model' => $modelTransl,
                'attribute' => 'description',
                'options' => [ // html attributes
                    'id'=>'content',
                    'value'=> $post['description']
                ],
                'clientOptions' => [
                    'toolbarInline' => false,
                    'theme' => 'royal', //optional: dark, red, gray, royal
                    'language' => 'en_gb' // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
                ]
            ]); ?>
            <!-- Source -->
            <?= $form->field($modelTransl, 'source')->textInput([ 'value' => $post['source'] ]) ?>
            <!-- Edit old one or create new translation -->
            <p>
                <?= Html::checkbox("edit_checkbox", false, ['0' => 'Edit', '1' => 'Translation']) ?>
                <span>Check if this is new translation to already existing post, If not this will edit post.</span>
            </p>
            <!-- Submit button -->
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
