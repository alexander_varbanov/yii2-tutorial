<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap4;
use yii\bootstrap;
?>
<!-- DE list of articles -->
<table class="table table-striped">
    <thead>
    <tr>
        <th>Datum</th>
        <th>Header</th>
        <th>Kurze beschreibung</th>
        <th>Mehr</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post['date_created'] ?></td>
            <td><?= $post['title'] ?></p></td>
            <td><?= $post['short_description'] ?></td>
            <td>
                <?= Html::a('Einzelheiten', ['/site/detail', 'id' => $post['post_id'], 'language_id' => '3' ], ['class'=>'btn btn-info']) ?>
                <?= Html::a('Bearbeiten', ['/site/edit', 'id' => $post['post_id'], 'language_id' => '3' ], ['class'=>'btn btn-info']) ?>
                <?= Html::a('Löschen', ['/site/delete', 'id' => $post['id'] ], [ 'class'=>'btn btn-info',
                            'data' => [ 'confirm' => 'Are you sure you want to delete it?', 'method' => 'post'] ] )?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
