<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap4;
use yii\bootstrap;
?>
<!-- BG list of articles -->
<table class="table table-striped">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Заглавие</th>
        <th>Кратко описание</th>
        <th>Виж повече</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post['date_created'] ?></td>
            <td><?= $post['title'] ?></p></td>
            <td><?= $post['short_description'] ?></td>
            <td>
                <?= Html::a('Детайли', ['/site/detail', 'id' => $post['post_id'], 'language_id' => '1' ], ['class'=>'btn btn-info']) ?>
                <?= Html::a('Редактирай', ['/site/edit', 'id' => $post['post_id'], 'language_id' => '1' ], ['class'=>'btn btn-info']) ?>
                <?= Html::a('Изтрий', ['/site/delete', 'id' => $post['id'] ], [ 'class'=>'btn btn-info',
                                        'data' => [ 'confirm' => 'Are you sure you want to delete it?', 'method' => 'post'] ] )?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>