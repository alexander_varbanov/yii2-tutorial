<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap4;
use yii\bootstrap;

?>
<!-- Language -->
<div>
    <p>
        <?php foreach ($languagesArr as $languageId => $languageName): ?>
            <?= Html::a($languageName, ['/site/admin', 'language_id' => $languageId ], ['class'=>'btn btn-light']) ?>
        <?php endforeach; ?>
    </p>
</div>
<!-- End language -->
<!-- Add new article -->
<div>
    <p><?= Html::a('+', ['/site/create' ], ['class'=>'btn btn-info']) ?></p>
</div>
<!-- End add new article -->
<!-- Posts list -->
<?php if ($tableLang == '1'): ?>
    <?= $this->render('partials/tableAdminBg', ['posts' => $posts]) ?>
<?php elseif ($tableLang == '2'): ?>
    <?= $this->render('partials/tableAdminEn', ['posts' => $posts]) ?>
<?php elseif ($tableLang == '3'): ?>
    <?= $this->render('partials/tableAdminDe', ['posts' => $posts]) ?>
<?php endif; ?>
<!-- End post list -->
<!-- Pagination -->
<?= LinkPager::widget(['pagination' => $pagination]) ?>



