<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap4\ActiveForm;
use codezeen\yii2\tinymce\TinyMce;
use yii\jui\DatePicker;
?>

<p><?= Html::a('<<<', ['/site/admin' ], ['class'=>'btn btn-info']) ?></p>
<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'create-form']); ?>
            <!--Date-->
            <?= $form->field($modelPost, 'date_created')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control'],
                'dateFormat' => 'dd-MM-yyyy'
            ])?>
            <!-- Language -->
            <?= $form->field($modelTransl, 'language_id')->dropdownList($languagesArr)->label('Language'); ?>
            <!-- Title -->
            <?= $form->field($modelTransl, 'title')->textInput()->label('Title') ?>
            <!-- Short Description -->
            <?= $form->field($modelTransl, 'short_description')->textarea(); ?>
            <!-- Description -->
            <?= $form->field($modelTransl, 'description')->widget(
                TinyMce::className(),
                [
                    'settings'        => [
                        'language'               => 'en',
                        'plugins'                => [
                            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                            "searchreplace visualblocks visualchars code fullscreen",
                            "insertdatetime media nonbreaking save table contextmenu directionality",
                            "template paste textcolor"
                        ],
                        'toolbar'                => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor",
                        'toolbar_items_size'     => 'small',
                        'image_advtab'           => true,
                        'relative_urls'          => false,
                    ],
                ]
            ) ?>
            <!-- Source -->
            <?= $form->field($modelTransl, 'source')->textInput() ?>
            <!-- Submit button -->
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

