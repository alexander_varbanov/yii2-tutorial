<?php
use yii\helpers\Html;
?>
<!-- Language -->
<div>
    <p>
        <?php foreach ($languagesArr as $languageId => $languageName): ?>
            <?= Html::a($languageName, ['/post/detail', 'id' => $post->post_id, 'language_id' => $languageId ], ['class'=>'btn btn-light']) ?>
        <?php endforeach; ?>
    </p>
</div>
<!--End language-->
<!-- Post details -->
<p><?= Html::a('<<<', ['/post/index' ], ['class'=>'btn btn-info']) ?></p>
<table class="table">
    <tr>
        <td><?= $post->title ?></td>
    </tr>
    <tr>
        <td><?= $post->date_created ?></td>
    </tr>
    <tr>
        <td><?= $post->description ?></td>
    </tr>
    <tr>
        <td><?= $post->source ?></td>
    </tr>
</table>
<!-- End post details -->