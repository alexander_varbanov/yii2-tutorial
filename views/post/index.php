<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<!-- Language -->
<div>
    <p>
        <?php foreach ($languagesArr as $languageId => $languageName): ?>
            <?= Html::a($languageName, ['/post/index', 'language_id' => $languageId ], ['class'=>'btn btn-light']) ?>
        <?php endforeach; ?>
    </p>
</div>
<!-- End language -->
<!-- Posts list -->
<?php if ($tableLang == '1'): ?>
    <?= $this->render('partials/tableBg', ['posts' => $posts]) ?>
<?php elseif ($tableLang == '2'): ?>
    <?= $this->render('partials/tableEn', ['posts' => $posts]) ?>
<?php elseif ($tableLang == '3'): ?>
    <?= $this->render('partials/tableDe', ['posts' => $posts]) ?>
<?php endif; ?>
<!-- End post list -->
<!-- Pagination -->
<?= LinkPager::widget(['pagination' => $pagination]) ?>


