<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<!-- EN list of articles -->
<table class="table table-striped">
    <thead>
    <tr>
        <th>Date</th>
        <th>Header</th>
        <th>Short Description</th>
        <th>More</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post['date_created'] ?></td>
            <td><?= $post['title'] ?></p></td>
            <td><?= $post['short_description'] ?></td>
            <td>
                <?= Html::a('Details', ['/post/detail', 'id' => $post['post_id'], 'language_id' => '2' ], ['class'=>'btn btn-info']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

