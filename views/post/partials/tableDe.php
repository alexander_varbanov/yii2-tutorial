<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<!-- DE list of articles -->
<table class="table table-striped">
    <thead>
    <tr>
        <th>Datum</th>
        <th>Header</th>
        <th>Kurze beschreibung</th>
        <th>Mehr</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post['date_created'] ?></td>
            <td><?= $post['title'] ?></p></td>
            <td><?= $post['short_description'] ?></td>
            <td>
                <?= Html::a('Einzelheiten', ['/post/detail', 'id' => $post['post_id'], 'language_id' => '3' ], ['class'=>'btn btn-info']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

