<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<!-- BG list of articles -->
<table class="table table-striped">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Заглавие</th>
        <th>Кратко описание</th>
        <?= Html::tag('th', Html::encode('Виж повече')) ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post['date_created'] ?></td>
            <td><?= $post['title'] ?></p></td>
            <td><?= $post['short_description'] ?></td>
            <td>
                <?= Html::a('Детайли', ['/post/detail', 'id' => $post['post_id'], 'language_id' => '1' ], ['class'=>'btn btn-info']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
